import { Component } from '@angular/core';
import { OCRResult } from '@ionic-native/ocr/ngx';
import { UtilitiesService } from '../services/utilities/utilities.service';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {

  loading = false;
  results = [];

  constructor(private utils: UtilitiesService) {}

  captureImage(): void {
    this.utils.captureImage().then((imageData: any) => {
      this.ocrScan(imageData);
    });
  }

  ocrScan(imageData: any): void {
    this.loading = true;
    this.utils.ocrScan(imageData).then((result: OCRResult) => {
      this.results = this.utils.filterLicencePlate(result.lines.linetext);
      this.loading = false;
    }).catch((e: any) => {
      this.loading = false;
      console.error(e);
    });
  }

}
