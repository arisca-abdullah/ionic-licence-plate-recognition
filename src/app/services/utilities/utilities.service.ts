import { Injectable } from '@angular/core';
import { Platform } from '@ionic/angular';
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';
import { OCR, OCRResult, OCRSourceType } from '@ionic-native/ocr/ngx';

@Injectable({
  providedIn: 'root'
})
export class UtilitiesService {

  licencePlatePattern: RegExp = /[A-Z]{1,2}\s?[0-9]{1,4}\s?[A-Z]{1,3}/g;
  licencePlatePatternParts: RegExp[] = [/[A-Z]{1,2}/g, /[0-9]{1,4}/g, /[A-Z]{1,3}/g];
  cameraOptions: CameraOptions = { quality: 99 };

  constructor(
    private platform: Platform,
    private camera: Camera,
    private ocr: OCR,
  ) { }

  async platformReady(callback: () => any): Promise<any> {
    await this.platform.ready();
    return callback();
  }

  captureImage(): Promise<any> {
    return this.platformReady(() => this.camera.getPicture(this.cameraOptions));
  }

  ocrScan(imageData: string): Promise<OCRResult> {
    return this.platformReady(() => this.ocr.recText(OCRSourceType.NORMFILEURL, imageData));
  }

  filterLicencePlate(data: string[]): string[] {
    return data
      .filter((d: string) => this.licencePlatePattern.test(d))
      .map((d: string) => d.match(this.licencePlatePattern)[0])
      .map((d: string) => {
        const result: { source: string, data: string }[] = [];

        this.licencePlatePatternParts.forEach((pattern: RegExp) => {
          const source = result.length ?
            result[result.length - 1].source.substring(result[result.length - 1].data.length).trim() : d;
          result.push({ source, data: source.match(pattern)[0] });
        });

        return result.map(res => res.data).join(' ');
      });
  }

}
